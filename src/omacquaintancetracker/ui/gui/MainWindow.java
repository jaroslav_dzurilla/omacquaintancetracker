package omacquaintancetracker.ui.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JFrame;

import omacquaintancetracker.datamanagement.DataWrap;
import omacquaintancetracker.ui.Message;
import omacquaintancetracker.ui.UserInterface;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainWindow extends UserInterface {
	
	private JFrame window;

	public MainWindow(DataWrap dataPack, ConcurrentLinkedQueue<Message> messages) {
		super(dataPack, messages);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	protected void initialize() {
		this.window	= new JFrame();
		this.window.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				MainWindow.this.killThisUI();
				super.windowClosing(e);
			}
		});
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.window.setTitle("Acquaintance Tracker");
		this.window.setSize( 600, 200);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.window.setLocation(dim.width/2-this.window.getSize().width/2, dim.height/2-this.window.getSize().height/2);
		
		this.window.setVisible(true);
	}


	@Override
	protected void mainLoopBody() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void reactionToUnexpected(Message message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void reactionToTest(Message message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void reactionToSourceFileChanged(Message message) {
		// TODO Auto-generated method stub

	}
	

	@Override
	protected void reactionToSaved(Message message) {
		// TODO Auto-generated method stub

	}
	

	@Override
	protected void reactionToLoaded(Message message) {
		// TODO Auto-generated method stub

	}
	

	@Override
	protected void reactionToErrorMessage(Message message) {
		new ErrorWindow( message.getMessageContent());		
	}

}
