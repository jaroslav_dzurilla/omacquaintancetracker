package omacquaintancetracker.ui.gui;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import java.awt.Window.Type;
import java.awt.Font;
import java.awt.Toolkit;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

public class ErrorWindow  {
	private JFrame window;
	private JButton btnOKButton;
	private JTextArea txtpnErrorMessage;
	private JScrollBar scrollBar;
	
	private String ErrorMessage;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize(){
		this.window	= new JFrame();
		this.window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.window.setLocation(dim.width/2-this.window.getSize().width/2, dim.height/2-this.window.getSize().height/2);
		this.window.setSize( 400, 400);

		this.window.setType(Type.POPUP);
		this.window.setResizable(false);
		this.window.setTitle("Error");
		
		this.btnOKButton = new JButton("OK");
		this.btnOKButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ErrorWindow.this.window.setVisible(false);
			}
		});
		
		this.btnOKButton.setSize( 100, 50);
		this.window.getContentPane().add(btnOKButton, BorderLayout.SOUTH);
		
		this.txtpnErrorMessage = new JTextArea();
		txtpnErrorMessage.setLineWrap(true);
		this.txtpnErrorMessage.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.txtpnErrorMessage.setEditable(false);
		this.txtpnErrorMessage.setText("Error message");
		this.txtpnErrorMessage.setText( this.ErrorMessage );
//		this.scrollBar = new JScrollBar();
//		this.scrollBar.add(txtpnErrorMessage);
//		window.getContentPane().add(scrollBar, BorderLayout.CENTER);
		this.window.getContentPane().add(txtpnErrorMessage, BorderLayout.CENTER);
	}
	
	public ErrorWindow( String message){
		
		this.ErrorMessage	= new String( message);
		
		this.initialize();
		
		this.show();
	}
	
	public void show(){
		this.window.setVisible(true);
	}

}
