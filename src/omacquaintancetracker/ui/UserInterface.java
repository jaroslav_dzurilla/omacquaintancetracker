package omacquaintancetracker.ui;

import java.util.concurrent.ConcurrentLinkedQueue;

import omacquaintancetracker.datamanagement.DataWrap;

/**
 * Define core functionality for UI.
 * @author JR-tiq - Jaroslav Dzurilla
 *
 */
public abstract class UserInterface extends Thread{
	protected DataWrap dataPack;
	private ConcurrentLinkedQueue<Message> sentMessages;
	private ConcurrentLinkedQueue<Message> receivedMessages;
	private boolean isRunning	=	false;
	
	protected UserInterface( DataWrap dataPack, ConcurrentLinkedQueue<Message> messages){
		this.dataPack	= dataPack;
		this.sentMessages	= messages;
		this.isRunning		= true;
		this.receivedMessages	= new ConcurrentLinkedQueue<Message>();
	}
	
	public void sendMessage( Message message){
		this.receivedMessages.add( message);
	}
	
	protected void sendMessageSave(){
		this.sentMessages.add( new Message( Thread.currentThread().getId(), Message.MessageType.Save, ""));
	}
	
	protected void sendMessageLoad(){
		this.sentMessages.add( new Message( Thread.currentThread().getId(), Message.MessageType.Load, ""));
	}
	
	protected void sendMessageChangeFile( String filePath){
		this.sentMessages.add( new Message( Thread.currentThread().getId(), Message.MessageType.ChangeSourceFile, filePath));
	}
	
	public void run(){
		
		this.initialize();
		
		do{
			this.mainLoopBody();
			
			if( this.receivedMessages.isEmpty()) continue;
				else this.messageHandling( this.receivedMessages.poll());
						
		}while( this.isRunning());
	}
	
	public boolean isRunning(){
		return this.isRunning;
	}
	
	public void killThisUI(){
		this.isRunning	= false;
	}
	
	protected void messageHandling( Message message){
		
		switch( message.getMessageType()){
		
		case Test : this.reactionToTest(message);
		
		case SourceFileChanged : this.reactionToSourceFileChanged(message);
		
		case Saved : this.reactionToSaved(message);
		
		case Loaded : this.reactionToLoaded(message);
		
		case ErrorMessage : this.reactionToErrorMessage(message);
		
		default: this.reactionToUnexpected(message);
			break;
		
		}
	}
	
	protected abstract void initialize();
	
	protected abstract void mainLoopBody();
	
	protected abstract void reactionToUnexpected( Message message);
	
	protected abstract void reactionToTest( Message message);
	
	protected abstract void reactionToSourceFileChanged( Message message);
	
	protected abstract void reactionToSaved( Message message);
	
	protected abstract void reactionToLoaded( Message message);
	
	protected abstract void reactionToErrorMessage( Message message);

}
