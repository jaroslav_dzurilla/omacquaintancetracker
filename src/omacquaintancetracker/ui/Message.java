package omacquaintancetracker.ui;

/**
 * Store content of communication between threads.
 * @author JR-tiq - Jaroslav Dzurilla
 *
 */
public class Message {
	// TODO insert data
	private String messageContent;
	private long senderThredId;
	private MessageType	messageType; 
	private Class<?> TargetObject;	
	
	public enum MessageType{
		Test, Tested, ChangeSourceFile, SourceFileChanged, Save, Saved, Load, Loaded, ErrorMessage
	}
	
	public Message( long senderThreadId, MessageType messageType, String messageContend){
		this.senderThredId	= senderThreadId;
		this.messageContent	= messageContend;
		this.messageType	= messageType;
	}
	
	public Message setTargetObject( Class<?> TargetObject){
		this.TargetObject	= TargetObject;
		return this;
	}
	
	public Class<?> getTargetObject(){
		return this.TargetObject;
	}
	
	public String getMessageContent(){
		return this.messageContent;
	}
	
	public long getSenderId(){
		return this.senderThredId;
	}
	
	public MessageType getMessageType(){
		return this.messageType;
	}
}
