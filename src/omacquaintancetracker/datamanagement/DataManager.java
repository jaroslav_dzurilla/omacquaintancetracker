package omacquaintancetracker.datamanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import omacquaintancetracker.xml.classes.DATA;

/**
 * DataManager produces data and encapsulates JAXB operations unmarshaling and marshaling, and file accessing.
 * @author JR-tiq - Jaroslav Dzurilla
 *
 */
public class DataManager {
	
	static final String DEFAULT_DATA_FILE = "./OMAcquaintancesData.xml";
	
	private JAXBContext jaxbContext;
	private Marshaller jaxbMarshaller;
    private Unmarshaller jaxbUnmarshaller;
    private String path;
    private Schema schema ;
	
	private void jaxbInit() throws JAXBException, SAXException{
		this.jaxbContext = JAXBContext.newInstance(DATA.class);
		this.jaxbMarshaller = jaxbContext.createMarshaller();
		this.jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		SchemaFactory schemaFactory = SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI);
		this.schema = schemaFactory.newSchema( this.getClass().getResource( "/XMLschema/OMAcquaintanceTrackerXMLschema.xsd"));
		this.jaxbMarshaller.setSchema( this.schema);
		this.jaxbUnmarshaller.setSchema( this.schema);
	}
    
    public DataManager() throws JAXBException, SAXException{
    	this.jaxbInit();
    	this.path = DEFAULT_DATA_FILE;
	}
    
    public DataManager( String DataFilePath) throws JAXBException, SAXException{
    	this.jaxbInit();
    	this.path =	DataFilePath;
    }
	
    public void setAnotherDataFile( String path) throws FileNotFoundException{
    	if( DataManager.doesFileExist( path)) this.path	= path;
    	else throw new FileNotFoundException();
    }
    
	public DATA	load() throws JAXBException, IOException {
		DATA data = null;

		FileInputStream fis = new FileInputStream( this.path);
		data =	(DATA) jaxbUnmarshaller.unmarshal(fis);
		fis.close();
		
		return data;
	}
	
	public void save( DATA data) throws JAXBException, IOException{
		FileOutputStream fos = new FileOutputStream( this.path);
		jaxbMarshaller.marshal( data, fos);
		fos.flush();
		fos.close();
	}
	
	@SuppressWarnings("resource")
	public void createNewDataFile() {
		try{
			new FileOutputStream( this.path);
		}catch( FileNotFoundException e){
			//Should by thrown if no file was found but also a new one should by created which is point of this method so this exception is muted
		}
	}
	
	public boolean doesFileExist(){
		return DataManager.doesFileExist( this.path);
	}
	
	public static boolean doesFileExist( String path){
		return (new File( path)).isFile();
	}

}
