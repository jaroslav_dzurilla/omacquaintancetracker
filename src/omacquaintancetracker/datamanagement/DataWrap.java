package omacquaintancetracker.datamanagement;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import omacquaintancetracker.xml.classes.DATA;

public class DataWrap {
	private DATA data	= 	null;
	public final ReentrantReadWriteLock	dataLock = new ReentrantReadWriteLock( true);
	
	public DATA getData(){
		return this.data;
	}
	
	public void setData( DATA data){
		this.data	= data;
	}
	
	public DataWrap(){
		super();
	}
	
	public DataWrap( DATA data){
		this.data	= data;
	}
	
}
