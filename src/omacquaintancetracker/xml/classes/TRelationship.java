//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.17 at 04:44:47 PM GMT 
//


package omacquaintancetracker.xml.classes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TRelationship.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TRelationship">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="neutral"/>
 *     &lt;enumeration value="friend"/>
 *     &lt;enumeration value="caution"/>
 *     &lt;enumeration value="foe"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TRelationship")
@XmlEnum
public enum TRelationship {

    @XmlEnumValue("neutral")
    NEUTRAL("neutral"),
    @XmlEnumValue("friend")
    FRIEND("friend"),
    @XmlEnumValue("caution")
    CAUTION("caution"),
    @XmlEnumValue("foe")
    FOE("foe");
    private final String value;

    TRelationship(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TRelationship fromValue(String v) {
        for (TRelationship c: TRelationship.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
