/**
 * 		Small-scale xml based personal database to keep a track of online multiplayer acquaintances and to distinguish friends from foes.
 * 		@author JR-tiq - Jaroslav Dzurilla
 * 		by 2017
 * 		
 */
package omacquaintancetracker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.xml.bind.JAXBException;

import org.omg.IOP.Encoding;
import org.xml.sax.SAXException;

import omacquaintancetracker.datamanagement.DataManager;
import omacquaintancetracker.datamanagement.DataWrap;
import omacquaintancetracker.ui.Message;
import omacquaintancetracker.ui.Message.MessageType;
import omacquaintancetracker.ui.UserInterface;
import omacquaintancetracker.ui.gui.MainWindow;
import sun.misc.IOUtils;

/**
 * Launcher is main object of application.
 * It governs its data and manages its threads.
 * @author JR-tiq - Jaroslav Dzurilla
 *
 */
public class Launcher {
	
	/**
	 * Encapsulates constants to use as exit codes for application.
	 * @author JR-tiq - jaroslav Dzurilla
	 *
	 */
	public class EXITCODES {
		public static final int ENDED_NORMALY				= 0;
		public static final int	UNHANDLED_ERROR				= 1;
		public static final int	FAILED_JAXB_INIT			= 2;	// Something wrong with DATA class messes up with JAXBContext
		public static final int	FAILED_XSD_AQUZITION		= 3;	// Failed to acquire xsd file
		public static final int FAILED_TO_OPEN_DATA_FILE	= 4;	// Failed to open data file
		public static final int INVALID_XML_FILE			= 5;	// File is not valid
		public static final int INVALID_DATA_FORMAT			= 6;	// Data is not valid against schema
	}
	
	public static final int TIMEOUT_COUNT	= 5;	// How many times to try to do something before it's pointless to try anymore

	protected 	DataManager dataManager;
	
	protected	DataWrap	dataPack 			= new DataWrap();
	
	protected	ConcurrentLinkedQueue<Message>	messenger	= new ConcurrentLinkedQueue<Message>();
	
	protected 	List<UserInterface> userInterfaces			= new ArrayList<UserInterface>();
	
	protected void init(){
		this.dataPack	= new DataWrap();
		this.messenger	= new ConcurrentLinkedQueue<Message>();
		this.userInterfaces	= new ArrayList<UserInterface>();
	}
	
	Launcher( String[] args){
		this.initDataManager(args);
	}
	
	Launcher( DataManager dataManager){
		this.dataManager	= dataManager;	
	}
	
	public static void main( String[] args) throws IOException {
		
		
		Launcher app	= new Launcher(args);
		
		app.dataPack.dataLock.writeLock().lock();
		try{
			app.loadData();
		}finally{	
			app.dataPack.dataLock.writeLock().unlock();
		}
		
		//do{}while();
		
		app.userInterfaces.add( new MainWindow( app.dataPack, app.messenger));
		
		for( UserInterface UI : app.userInterfaces)UI.start();
		System.out.println("UI started");
		
		FileInputStream fis = new FileInputStream( "./OMAcquaintancesData.xml");		

		String inputStreamString = new Scanner(fis,"UTF-8").useDelimiter("\\A").next();
		
		fis.close();
		
		System.out.println( inputStreamString);
		
		for( UserInterface UI : app.userInterfaces)UI.sendMessage( new Message(Thread.currentThread().getId(), MessageType.ErrorMessage, inputStreamString));
				
		System.out.println("Message sent to UI");	
		for( UserInterface UI : app.userInterfaces)
			try {
				System.out.println("Before join");
				UI.join();
				System.out.println("After join");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		// TODO This is where magic will happen
		
		
		app.dataPack.dataLock.readLock().lock();
		try{
			app.saveData();
		}finally{
			app.dataPack.dataLock.readLock().unlock();
		}
		
		System.out.println("END");
		System.exit(Launcher.EXITCODES.ENDED_NORMALY);
		
	}
	
	private void messageProccessing(){
		
	}
	
 	private void initDataManager( String[] args){
		try{
			
			if( args.length == 0) dataManager = new DataManager();
			else dataManager = new DataManager( args[0]);
			
		} catch ( SAXException e) {	
			// Failed to acquire xsd file
			// TODO Error message and terminate
			// For now
			 
			e.printStackTrace();
			System.exit(Launcher.EXITCODES.FAILED_XSD_AQUZITION);
			
		} catch (JAXBException e) {
			// Something wrong with DATA class messes up with JAXBContext
			// TODO Error message and terminate
			// For now
			 
			e.printStackTrace();
			System.exit(Launcher.EXITCODES.FAILED_JAXB_INIT);
		}
	}

	private void loadData(){
		
		boolean dataLoaded = false;
		
		for( int i=0; ( i<= Launcher.TIMEOUT_COUNT )&&( !dataLoaded ); i++){
				
			try{

				this.dataPack.setData( dataManager.load());
				dataLoaded	= true;

			} catch( FileNotFoundException e){
				
				dataLoaded	= false;
				// for now
				e.printStackTrace();
				
				if( !this.dataManager.doesFileExist()){
					// File doesn't exist
					// TODO Dialog to ask if user wants open different file or create new
					this.dataManager.createNewDataFile();	// temporal default solution

				}else{
					// Access to file is denied
					// TODO Dialog to ask if user wants to open different file or exit

					System.exit( Launcher.EXITCODES.FAILED_TO_OPEN_DATA_FILE);
				}
				
				
			} catch (IOException e) {
				// Some error occurred while closing fileInputStream
				// TODO Error window
				e.printStackTrace();
			} catch( JAXBException e){
				
				dataLoaded	= false;
				// Validation failed.
				// TODO when available launch dialog to inform user about broken file and ask for another file.
				// and/or
				// TODO Implement custom validation event handler to figure what went wrong	
				// but for now  
				 
				e.printStackTrace();

				System.exit( Launcher.EXITCODES.INVALID_XML_FILE);
				
			}
		}
		if( !dataLoaded) System.exit(Launcher.EXITCODES.FAILED_TO_OPEN_DATA_FILE );
	
	}
	
	private void saveData(){

		boolean dataSaved = false;
		
		for( int i=0; (i<= Launcher.TIMEOUT_COUNT )&&( !dataSaved ); i++){

			try {
				
				this.dataManager.save(this.dataPack.getData());
				dataSaved	= true;
				
			} catch (FileNotFoundException e) {
				
				dataSaved	= false;
				// Access denied
				// TODO Error message + dialog to ask for another file to save to.
				e.printStackTrace();
				System.exit( Launcher.EXITCODES.FAILED_TO_OPEN_DATA_FILE);
				
			} catch (IOException e) {
				// Some error occurred while closing fileOutputStream
				// TODO Error window
				e.printStackTrace();
			} catch (JAXBException e) {
				
				dataSaved	= false;
				// data doesn't have valid format and cannot be marshaled. Check IDs.
				// TODO Error message but it's applications fault.
				e.printStackTrace();
				System.exit( Launcher.EXITCODES.INVALID_DATA_FORMAT);
				
			}
		}
			
	}
	
	
}
